/*
 * Copyright (C) 2012-2014 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/
#include <boost/bind.hpp>
#include <boost/algorithm/string.hpp>
#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/common/common.hh>
#include <gazebo/physics/ContactManager.hh>

#include <stdio.h>
#include <thread>
#include "ros/ros.h"
#include "ros/callback_queue.h"
#include "ros/subscribe_options.h"
#include "std_msgs/Bool.h"
#include "std_msgs/Float32.h"
#include "std_msgs/String.h"
#include <sensor_msgs/Joy.h>

#include "ADVR_ROS/advr_grasp_control.h"


namespace gazebo {
    class ModelPush : public ModelPlugin {
    public:
        void Load(physics::ModelPtr _parent, sdf::ElementPtr _sdf) {

            // get link name
            if (!_sdf->HasElement("link_name")) {
                std::cout << "MagneticPlugin: Cannot load the GazeboGraspFix without any <link_name> params"
                          << std::endl;
            } else {
                link_name = _sdf->GetElement("link_name")->Get<std::string>();
                std::cout << std::endl;
                std::cout << "----------------------------------------------------------------------------------" << std::endl;
                std::cout << "----------------------------------------------------------------------------------" << std::endl;
                std::cout << "Successfully load gazebo_magnetic_attach_plugin for: " << _parent->GetName() << "::" << link_name << std::endl << std::endl;

//                std::cout << "-----------------" << std::endl;
//                std::cout << "*** AutoGrasp ***" << std::endl;
//                std::cout << "-----------------" << std::endl;
//                std::cout << "To grasp an object: " << std::endl;
//                std::cout << "	$ rostopic pub /grasp/" << link_name << "/autoGrasp std_msgs/Bool \"data: true\"" << std::endl << std::endl;
//                std::cout << "To detach the object: " << std::endl;
//                std::cout << "	$ rostopic pub /grasp/" << link_name << "/autoGrasp std_msgs/Bool \"data: false\"" << std::endl << std::endl;
//                std::cout << "Note: " << std::endl;
//                std::cout << "	AutoGrasp: Make sure this link is in contact with the object to be grasped!" << std::endl;
//                std::cout << "	           Otherwise, no attachment will be established!" << std::endl;
//
//                std::cout << "-----------------" << std::endl;
//                std::cout << "*** GoalGrasp ***" << std::endl;
//                std::cout << "-----------------" << std::endl;
//                std::cout << "To grasp an object: " << std::endl;
//                std::cout << "	$ rostopic pub /grasp/" << link_name << "/goalGrasp std_msgs/String \"data: \'model::link\'\"" << std::endl << std::endl;
//                std::cout << "To detach the object: " << std::endl;
//                std::cout << "	$ rostopic pub /grasp/" << link_name << "/goalGrasp std_msgs/String \"data: \'\'\"" << std::endl << std::endl;
//                std::cout << "Note: " << std::endl;
//                std::cout << "	GoalGrasp: No specific contact needed! The attachment will be established at current distance!" << std::endl;
//                std::cout << "----------------------------------------------------------------------------------" << std::endl;
//                std::cout << "----------------------------------------------------------------------------------" << std::endl;
//                std::cout << std::endl;

                std::cout << "---------------------------------------" << std::endl;
                std::cout << "*** How to: ***" << std::endl;
                std::cout << "---------------------------------------" << std::endl;
                std::cout << "To grasp an object: " << std::endl;
                std::cout << "	$ rosservice call /grasp/" << link_name << " \"grasp: 1\"" << std::endl << std::endl;
                std::cout << "To detach the object: " << std::endl;
                std::cout << "	$ rosservice call /grasp/" << link_name << " \"grasp: 0\"" << std::endl << std::endl;
                std::cout << "Service response: success (1 or 0)!" << std::endl << std::endl;
                std::cout << "Note: " << std::endl;
                std::cout << "  Make sure you have touched at least one object! " << std::endl;
                std::cout << "  The attachment will be established at current distance! " << std::endl;
                std::cout << "----------------------------------------------------------------------------------" << std::endl;
                std::cout << "----------------------------------------------------------------------------------" << std::endl;
                std::cout << std::endl;
            }

            // Store the pointer to the model
            model = _parent;

            // joint to be used to attach one model to another
            world = model->GetWorld();
            if (!world.get()) {
                std::cout << "world is NULL" << std::endl;
            }
            physics = model->GetWorld()->GetPhysicsEngine();
            fixedJoint = physics->CreateJoint("fixed");

            // Listen to the update event. This event is broadcast every simulation iteration.
            updateConnection = event::Events::ConnectWorldUpdateBegin(
                    boost::bind(&ModelPush::OnUpdate, this, _1));

            // Initialize ros, if it has not already bee initialized.
            if (!ros::isInitialized()) {
                int argc = 0;
                ros::init(argc, NULL, "gazebo_client", ros::init_options::NoSigintHandler);
            }

            // Create our ROS node. This acts in a similar manner to the Gazebo node
            rosNode.reset(new ros::NodeHandle("grasp"));

            // Create a named topic, and subscribe to it.
            std::string topicName;
            topicName = std::string("/grasp") + std::string("/") + link_name;
            topicName = link_name + std::string("/autoGrasp");
            ros::SubscribeOptions so = ros::SubscribeOptions::create<std_msgs::Bool>(topicName, 1,
                                                                                     boost::bind(&ModelPush::OnRosMsg,
                                                                                                 this, _1),
                                                                                     ros::VoidPtr(), &rosQueue);

            std::string topicName2;
            topicName2 = std::string("/grasp") + std::string("/") + link_name + std::string("/goal");
            topicName2 = link_name + std::string("/goalGrasp");
            ros::SubscribeOptions so2 = ros::SubscribeOptions::create<std_msgs::String>(topicName2, 1,
                                                                                     boost::bind(&ModelPush::OnRosMsg2,
                                                                                                 this, _1),
                                                                                     ros::VoidPtr(), &rosQueue2);

            rosSub = rosNode->subscribe(so);
            rosSub2 = rosNode->subscribe(so2);

            // service
            std::string service_name;
            service_name = std::string("/grasp") + std::string("/") + link_name;
            rosServiceServer = rosNode->advertiseService<ADVR_ROS::advr_grasp_controlRequest, ADVR_ROS::advr_grasp_controlResponse>(service_name,   boost::bind ( &ModelPush::OnService,
                                                                                                                                                                  this,
                                                                                                                                                                  _1, _2 ));



            // Spin up the queue helper thread.
            rosQueueThread = std::thread(std::bind(&ModelPush::QueueThread, this));
            rosQueueThread2 = std::thread(std::bind(&ModelPush::QueueThread2, this));

            // contact manager
            contact_manager = physics->GetContactManager();

            inContact = false;


            // initiate grasp state
            isGrasped = false;
            // publish grasp state
            std::string grasp_state_topic_name;
            grasp_state_topic_name = std::string("/grasp") + std::string("/") + link_name + std::string("/") + std::string("state");
            state_publisher = rosNode->advertise<std_msgs::Bool>(grasp_state_topic_name, 1);
        }





        // Called by the world update start event
    public:
        void OnUpdate(const common::UpdateInfo & /*_info*/) {
            // Apply a small linear velocity to the model (for testing purpose).
            //model->SetLinearVel(math::Vector3(0.1, 0, 0.0));

            // this enable service to work properly
            ros::spinOnce();

            // check contacts for this link and keep track of last object in contact with
            DetectContact();

            // publish grasp state
            std_msgs::Bool grasp_state_msg;
            grasp_state_msg.data = isGrasped;
            state_publisher.publish(grasp_state_msg);

        }


        /// check contacts of this link, and keep track of the last object that has contact with this link
        void DetectContact(){
            int num_contacts = contact_manager->GetContactCount();
            std::vector<physics::Contact *> contact_vector = contact_manager->GetContacts();
            for (int i = 0; i < num_contacts; i++) {
                if (contact_vector[i]->collision1->GetLink()->GetName() == link_name) {
                    modelNameInContactWith = contact_vector[i]->collision2->GetModel()->GetName();
                    linkNameInContactWith = contact_vector[i]->collision2->GetLink()->GetName();
                } else if (contact_vector[i]->collision2->GetLink()->GetName() == link_name) {
                    modelNameInContactWith = contact_vector[i]->collision1->GetModel()->GetName();
                    linkNameInContactWith = contact_vector[i]->collision1->GetLink()->GetName();
                }
            }
        }

        void Attach() {
            {
                inContact = false;
//                std::cout << "--------------------------------------------" << std::endl;
//                std::cout << "Global Contacts: " << std::endl;
                // collision detection, contact_manager gives all contacts of those links who's contact_sensor are enabled
//                std::cout << contact_manager->GetContactCount() << " contacts:" << std::endl;
                std::vector<physics::Contact *> contact_vector = contact_manager->GetContacts();
                if (contact_manager->GetContactCount() > 0) {
                    for (int i = 0; i < contact_manager->GetContactCount(); i++) {
//                        std::cout << "\t";
//                        std::cout << contact_vector[i]->collision1->GetModel()->GetName() << "::";
//                        std::cout << contact_vector[i]->collision1->GetLink()->GetName() << " ----- ";
//                        std::cout << contact_vector[i]->collision2->GetModel()->GetName() << "::";
//                        std::cout << contact_vector[i]->collision2->GetLink()->GetName() << std::endl;


                        if (contact_vector[i]->collision1->GetLink()->GetName() == link_name) {
                            inContact = true;
                            modelNameInContactWith = contact_vector[i]->collision2->GetModel()->GetName();
                            linkNameInContactWith = contact_vector[i]->collision2->GetLink()->GetName();

                        } else if (contact_vector[i]->collision2->GetLink()->GetName() == link_name) {
                            inContact = true;
                            modelNameInContactWith = contact_vector[i]->collision1->GetModel()->GetName();
                            linkNameInContactWith = contact_vector[i]->collision1->GetLink()->GetName();
                        }

                    }
                }


                if (inContact && !occupied_) {
                    std::cout << "Attach!" << std::endl;
//                    std::cout << modelNameInContactWith << "::" << linkNameInContactWith << " attached to "
//                              << model->GetName() << "::" << link_name << std::endl;
                    physics::ModelPtr obj_ptr = world->GetModel(modelNameInContactWith);
                    if (obj_ptr == NULL) std::cout << modelNameInContactWith << " not found" << std::endl;

                    gazebo::math::Pose diff =
                            obj_ptr->GetLink(linkNameInContactWith)->GetWorldPose() -
                            model->GetLink(link_name)->GetWorldPose();
                    fixedJoint->Load(model->GetLink(link_name), obj_ptr->GetLink(linkNameInContactWith), diff);
                    fixedJoint->Init();
                    fixedJoint->SetHighStop(0, 0);
                    fixedJoint->SetLowStop(0, 0);
                    isGrasped = true;
                    
                    occupied_ = true;
                } else {
//                    std::cout << link_name << " is not in contact with any object!" << std::endl;
                }
//                std::cout << "--------------------------------------------" << std::endl;


            }
        }

        void Detach() {
            {
//                std::cout << "--------------------------------------------" << std::endl;
               std::cout << "Detach!" << std::endl;
//                std::cout << "--------------------------------------------" << std::endl;
                fixedJoint->Detach();
                isGrasped = false;
                occupied_ = false;
            }
        }


        /// \brief Handle an incoming message from ROS
        /// \param[in] _msg A float value that is used to set the velocity of the Velodyne.
    public:
        void OnRosMsg(const std_msgs::Bool::ConstPtr &_msg) {

            // attach
            if (_msg->data == 1) {
                Attach();
            }

            // detach
            if (_msg->data == 0 && occupied_) {
                Detach();
            }
        }

        void OnRosMsg2(const std_msgs::String::ConstPtr &_msg) {
            using namespace std;

            if(_msg->data.empty()){
//                std::cout << "--------------------------------------------" << std::endl;
//                std::cout << "Detach!" << std::endl;
//                std::cout << "--------------------------------------------" << std::endl;
                fixedJoint->Detach();
                isGrasped = false;
            }else {

                std::string goal_model_link_name, goal_model_name, goal_link_name;
                goal_model_link_name = _msg->data;
                //std::cout << "goal_model_link_name: " << goal_model_link_name << std::endl;


                std::vector<std::string> strs;
                boost::split(strs, goal_model_link_name, boost::is_any_of("::"));

                //cout << "* size of the vector: " << strs.size() << endl;
                //for (vector<string>::iterator it = strs.begin(); it != strs.end(); ++it){cout << *it << endl;}

                if (strs.size() == 3) {
                    goal_model_name = *strs.begin();
                    goal_link_name = *(strs.end() - 1);


                    //std::cout << "Goal to be attached: " << goal_model_link_name << std::endl;
                    //std::cout << "\t goal_model_name: " << goal_model_name << std::endl;
                    //std::cout << "\t goal_link_name: " << goal_link_name << std::endl;

//                    std::cout << "--------------------------------------------" << std::endl;
//                    std::cout << "Attach!" << std::endl;
//                    std::cout << goal_model_name << "::" << goal_link_name << " attached to "
//                              << model->GetName() << "::" << link_name << std::endl;
//                    std::cout << "--------------------------------------------" << std::endl;

                    // Attach
                    physics::ModelPtr obj_ptr = world->GetModel(goal_model_name);
                    if (obj_ptr == NULL) std::cout << goal_model_name << " not found" << std::endl;

                    gazebo::math::Pose diff = obj_ptr->GetLink(goal_link_name)->GetWorldPose() -
                                              model->GetLink(link_name)->GetWorldPose();
                    fixedJoint->Load(model->GetLink(link_name), obj_ptr->GetLink(goal_link_name), diff);
                    fixedJoint->Init();
                    fixedJoint->SetHighStop(0, 0);
                    fixedJoint->SetLowStop(0, 0);
                    isGrasped = true;
                } else {
//                    cout << "please specify the model link name in the format \"goal_model_name::goal_link_name\"! "
//                         << endl;
                }
            }



        }

        bool OnService(ADVR_ROS::advr_grasp_controlRequest& req, ADVR_ROS::advr_grasp_controlResponse& res) {
            using namespace std;
            cout << "Grasp Service Called!" << endl;
//            string goal_model_name = req.model;
//            string goal_link_name = req.link;
            bool grasp = req.grasp;



            if (grasp==true){

                if (modelNameInContactWith == string("")){
                    std::cout << "-----------------------------------------------" << std::endl;
                    std::cout << "Make sure you have touched at least one object!" << std::endl;
                    std::cout << "-----------------------------------------------" << std::endl;
                    res.success = 0;
                } else {


                    std::cout << "--------------------------------------------" << std::endl;
                    std::cout << "Attach!" << std::endl;
                    std::cout << modelNameInContactWith << "::" << linkNameInContactWith << " attached to "
                              << model->GetName() << "::" << link_name << std::endl;
                    std::cout << "--------------------------------------------" << std::endl;

                    // Attach
                    physics::ModelPtr obj_ptr = world->GetModel(modelNameInContactWith);
                    if (obj_ptr == NULL) std::cout << modelNameInContactWith << " not found" << std::endl;

                    gazebo::math::Pose diff = obj_ptr->GetLink(linkNameInContactWith)->GetWorldPose() -
                                              model->GetLink(link_name)->GetWorldPose();
                    fixedJoint->Load(model->GetLink(link_name), obj_ptr->GetLink(linkNameInContactWith), diff);
                    fixedJoint->Init();
                    fixedJoint->SetHighStop(0, 0);
                    fixedJoint->SetLowStop(0, 0);
                    isGrasped = true;
                    res.success = 1;
                }

            }else if (grasp==false){
                std::cout << "--------------------------------------------" << std::endl;
                std::cout << "Detach!" << std::endl;
                std::cout << "--------------------------------------------" << std::endl;
                fixedJoint->Detach();
                isGrasped = false;
                res.success = 1;
            }



            return true;
        }

        /// \brief ROS helper function that processes messages
    private:
        void QueueThread() {
            static const double timeout = 0.01;
            while (rosNode->ok()) {
                rosQueue.callAvailable(ros::WallDuration(timeout));
            }
        }

        void QueueThread2() {
            static const double timeout = 0.01;
            while (rosNode->ok()) {
                rosQueue2.callAvailable(ros::WallDuration(timeout));
            }
        }

    private:
        bool occupied_ = false;

    private:
        physics::ModelPtr model;
        physics::PhysicsEnginePtr physics;
        physics::WorldPtr world;
        physics::JointPtr fixedJoint;
        std::string link_name;
        physics::ContactManager *contact_manager;

        bool inContact;
        bool isGrasped;
        std::string modelNameInContactWith;
        std::string linkNameInContactWith;


        /// \brief A node use for ROS transport
    private:
        std::unique_ptr<ros::NodeHandle> rosNode;

        /// \brief A ROS subscriber
    private:
        ros::Subscriber rosSub;
        ros::Subscriber rosSub2;

    private:
        ros::Publisher state_publisher;

    private:
        ros::ServiceServer rosServiceServer;

        /// \brief A ROS callbackqueue that helps process messages
    private:
        ros::CallbackQueue rosQueue;
        ros::CallbackQueue rosQueue2;

        /// \brief A thread the keeps running the rosQueue
    private:
        std::thread rosQueueThread;
        std::thread rosQueueThread2;

        // Pointer to the update event connection
    private:
        event::ConnectionPtr updateConnection;
    };

    // Register this plugin with the simulator
    GZ_REGISTER_MODEL_PLUGIN(ModelPush)
}
